use serde::{Serialize, Deserialize};
use chrono::{DateTime, Utc};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Link {
    pub id: String,
    pub destination: String,
    pub created: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct AddLink {
    pub id: String,
    pub destination: String,
}

impl Into<Link> for AddLink {
    fn into(self) -> Link {
        Link {
            id: self.id,
            destination: self.destination,
            created: Utc::now(),
        }
    }
}
