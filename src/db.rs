use r2d2_sqlite::SqliteConnectionManager;
use rusqlite::params;

use crate::{Result, data};
use crate::data::Link;

#[derive(Clone)]
pub struct DbPool {
    pool: r2d2::Pool<SqliteConnectionManager>,
}

impl DbPool {
    pub fn new() -> Result<DbPool> {
        // r2d2 pool
        let manager = SqliteConnectionManager::file("rustlink.db");
        let pool = r2d2::Pool::new(manager).unwrap();

        Ok(DbPool {
            pool
        })
    }

    pub fn init_db(&self) -> Result<()> {
        let conn = self.pool.get()?;

        conn.execute(
            "CREATE TABLE IF NOT EXISTS links (\
                    ID TEXT PRIMARY KEY NOT NULL,\
                    destination TEXT NOT NULL UNIQUE,\
                    created DATETIME DEFAULT (datetime('now'))\
                    );",
            params![],
        ).unwrap();
        Ok(())
    }

    pub fn list_links(&self) -> Result<Vec<Link>> {
        let conn = self.pool.get()?;

        let mut stmt = conn.prepare("SELECT id, destination, created FROM links")?;
        let iter= stmt.query_map(params![], |row|
            Ok(Link {
                id: row.get(0)?,
                destination: row.get(1)?,
                created: row.get(2)?,
            }))?;
        let mut links = Vec::new();

        for link in iter {
            links.push(link?);
        }

        Ok(links)
    }

    pub fn get_link(&self, id: &str) -> Result<Option<Link>> {
        let conn = self.pool.get()?;

        let mut stmt = conn.prepare("SELECT id, destination, created FROM links WHERE id=?1")?;
        let mut iter= stmt.query_map(params![id], |row|
            Ok(Link {
                id: row.get(0)?,
                destination: row.get(1)?,
                created: row.get(2)?,
            }))?;
        if let Some(link) = iter.next() {
            Ok(Some(link?))
        } else {
            Ok(None)
        }
    }

    pub fn add_link(&self, link: data::AddLink) -> Result<()> {
        let conn = self.pool.get()?;

        let mut stmt = conn.prepare("INSERT INTO links (id, destination) VALUES (?1, ?2)")?;
        stmt.execute(params![&link.id, &link.destination])?;

        Ok(())
    }

    pub fn delete_link(&self, id: &str) -> Result<()> {
        let conn = self.pool.get()?;

        let mut stmt = conn.prepare("DELETE FROM links WHERE id = ?1")?;
        stmt.execute(params![id])?;

        Ok(())
    }
}
