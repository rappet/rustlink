use std::{fmt, io};
use std::error::Error as StdError;
use actix_web::{
    http::StatusCode,
    HttpResponse
};
use actix_web::error::BlockingError;

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    R2D2(r2d2::Error),
    Rusqlite(rusqlite::Error),
    Tera(tera::Error),
}

pub type Result<T> = std::result::Result<T, Error>;

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Io(err) => write!(f, "io: {}", err.description()),
            Error::R2D2(err) => write!(f, "database: {}", err.description()),
            Error::Rusqlite(err) => write!(f, "sqlite: {}", err.description()),
            Error::Tera(err) => write!(f, "tera: {}", err.description()),
        }
    }
}

impl actix_web::ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        error!("internal server error: {}", self.description());
        HttpResponse::new(StatusCode::INTERNAL_SERVER_ERROR)
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::Io(err) => err.source(),
            Error::R2D2(err) => Some(err),
            Error::Rusqlite(err) => Some(err),
            Error::Tera(err) => Some(err),
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<r2d2::Error> for Error {
    fn from(err: r2d2::Error) -> Error {
        Error::R2D2(err)
    }
}

impl From<rusqlite::Error> for Error {
    fn from(err: rusqlite::Error) -> Error {
        Error::Rusqlite(err)
    }
}

impl From<BlockingError<Error>> for Error {
    fn from(err: BlockingError<Error>) -> Error {
        match err {
            BlockingError::Error(err) => err,
            BlockingError::Canceled => "actix web worker canceled".into()
        }
    }
}

impl From<&'static str> for Error {
    fn from(err: &'static str) -> Error {
        io::Error::new(io::ErrorKind::Other, err).into()
    }
}

impl From<tera::Error> for Error {
    fn from(err: tera::Error) -> Error {
        Error::Tera(err)
    }
}
