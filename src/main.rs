extern crate actix_web;
extern crate actix_rt;
extern crate tera;
extern crate futures;
extern crate r2d2;
extern crate r2d2_sqlite;
extern crate rusqlite;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate serde;
extern crate chrono;

use actix_web::{web, get, post, Responder, HttpResponse, HttpServer, App};
use tera::{Context, Tera};

mod db;
mod error;
mod data;

pub use error::{Error, Result};
use crate::db::DbPool;

#[get("/")]
async fn index(tera: web::Data<Tera>, pool: web::Data<DbPool>) -> Result<impl Responder> {
    let links = web::block(move || {
        pool.list_links()
    }).await?;

    let mut context = Context::new();
    context.insert("links", &links);
    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body(tera.render("index.html", &context)?))
}

#[get("/{id}")]
async fn redirect(req: web::HttpRequest, pool: web::Data<DbPool>) -> Result<impl Responder> {
    let id = req.match_info().get("id").unwrap().to_string();
    let link = web::block(move || {
        pool.get_link(&id)
    }).await?;

    if let Some(link) = link {
        info!("redirect to {}", link.destination);

        Ok(HttpResponse::TemporaryRedirect()
            .header("location", link.destination)
            .body("Moved"))
    } else {
        Ok(HttpResponse::NotFound()
            .body("Redirect not found"))
    }
}

#[post("/add")]
async fn add(tera: web::Data<Tera>, pool: web::Data<DbPool>, add_link: web::Form<data::AddLink>) -> Result<impl Responder> {
    let link = add_link.into_inner();
    let link2 = link.clone();
    web::block(move || {
        pool.add_link(link2.clone())
    }).await?;
    info!("added link {} -> {}", link.id, link.destination);

    let mut context = Context::new();
    context.insert("link", &link);
    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body(tera.render("add.html", &context)?))
}

#[get("/delete/{id}")]
async fn delete(req: web::HttpRequest, tera: web::Data<Tera>, pool: web::Data<DbPool>) -> Result<impl Responder> {
    let id = req.match_info().get("id").unwrap().to_string();
    let id2 = id.clone();
    web::block(move || {
        pool.delete_link(&id2)
    }).await?;
    info!("deleted link with id {}", id);

    let mut context = Context::new();
    context.insert("id", &id);
    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body(tera.render("delete.html", &context)?))
}

#[actix_rt::main]
async fn main() -> Result<()> {
    std::env::set_var("RUST_LOG", "rustlink=info,actix_web=debug");
    env_logger::init();

    let pool = db::DbPool::new()?;
    pool.init_db()?;

    info!("start http server");

    HttpServer::new(move || {
        let tera = {
            let mut tera = Tera::new("templates/**/*").unwrap();
            tera.autoescape_on(vec!["html"]);
            tera
        };

        App::new()
            .data(tera)
            .data(pool.clone())
            .service(index)
            .service(redirect)
            .service(add)
            .service(delete)
    })
        .bind("[::]:8080")?
        .start()
        .await?;
    Ok(())
}
